﻿using System;
using MyApp1.Assignment1;

namespace MyApp1
{

    public class Program
    {
        static void Main(string[] args) 
        {
            while(true)
            {
                string s, ch;
                Console.WriteLine("\nPress 1 for Problem 1");
                Console.WriteLine("Press 2 for Problem 2");
                Console.WriteLine("Press 3 for Problem 3");
                Console.WriteLine("Press 4 for Problem 4");
                Console.Write("Enter your choice: ");
                ch = Console.ReadLine();
                switch (ch)
                {
                    case "1":
                        Console.WriteLine("\n\nProblem 1: Write a program and continuously ask the user to enter a number or \"ok\" to exit. \nCalculate the sum of all the previously entered numbers and display it on the console.\n\n");

                        s = Problem1.sum();
                        if(s != "NA")
                            Console.WriteLine($"\nSum of the element(s) is {s}");
                        break;
                
                    case "2":
                        Console.WriteLine("\n\nProblem 2: Write a program and ask the user to enter a series of numbers separated by \ncomma. \nFind the maximum of the numbers and display it on the console. For example, if the user \nenters \"5, 3 8, \n1, 4\", the program should display 8.\n\n");
            
                        s = Problem2.takeList();
                        if(s != "NA")
                            Console.WriteLine($"\nGreatest element is {s}");
                        break;
                
                    case "3":
                        Console.WriteLine("\n\nProblem 3: Write a program and ask the user to supply a list of comma separated \nnumbers (e.g 5, 1, 9, 2, 10). If the list is empty or includes less than 5 numbers, display \"Invalid \nList\" and ask the user to re-try; otherwise, display the 3 smallest numbers in the list.\n\n");
            
                        s = Problem3.takeList();
                        if(s != "NA")
                            Console.WriteLine($"\nSmallest 3 elements: {s}");
                        break;

                    case "4":
                        Console.WriteLine("\n\nProblem 4: Take comma-separated list of numbers as input. Print them in \ndescending order.\n\n");

                        s = Problem4.takeList();
                        if(s != "NA")
                            Console.WriteLine($"\nElements in descending order: {s}");
                        break;

                    default:
                        Console.WriteLine("You've entered a wrong choice!");
                        break;
                }

            }
        }
    }

}