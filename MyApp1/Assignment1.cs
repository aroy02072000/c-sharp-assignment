namespace MyApp1.Assignment1 
{

    public class Problem1 
    {
        public static string sum() {
            double s = 0;
            string str;
            try 
            {
                checked
                {
                    while(true)
                    {
                        Console.Write("Enter a number (Type \"ok\" to Exit): ");
                        str = Console.ReadLine();
                        if(str == "ok")
                            return Convert.ToString(s);
                        s += Convert.ToDouble(str);
                    }
                }
            }
            catch(System.Exception) 
            {
                Console.WriteLine("Invalid Input!!!");
                return "NA";
            }
        }
    }

    public class Problem2 
    {

        private static string getMax(string[] arr) 
        {
            string maxValue = arr[0];
            try
            {
                for(int i = 1; i < arr.Length; i++)
                {   checked 
                    {
                    if(Convert.ToDouble(maxValue) < Convert.ToDouble(arr[i]))
                        maxValue = arr[i];
                    }
                }
                return maxValue;
            }
            catch(System.Exception) 
            {
                Console.WriteLine("Invalid Input!!!");
                return "NA";
            }
        }

        public static string takeList() 
        {
            string s;
            try 
            {
                Console.Write("Enter the series of numbers separated by comma: ");
                s = Console.ReadLine();
                s = s.Replace(" ", "");
                string[] arr = s.Split(",");
                if(s == "")
                    throw new Exception();
                string maxValue = getMax(arr);
                return maxValue;
            }
            catch(System.Exception) 
            {
                Console.WriteLine("Invalid Input!!!");
                return "NA";
            }
        }
    }

    public class Problem3 
    {

        private static string getSmallestthreeItem(string[] arr) {
            try
            {
                if(arr.Length < 5) {
                    Console.WriteLine("Invalid List");
                    return "InvalidListException";
                }
                for(int i = 0; i < arr.Length - 1; i++) {
                    for(int j = 0; j < arr.Length - i - 1; j++) {
                        checked 
                        {
                            if(Convert.ToDouble(arr[j]) > Convert.ToDouble(arr[j + 1])) {
                                string temp = arr[j];
                                arr[j] = arr[j + 1];
                                arr[j + 1] = temp;
                            }
                        }
                    }
                }
                ArraySegment<string> newArr = new ArraySegment<string>(arr, 0, 3);
                return string.Join(", ", newArr);
            }
            catch(System.Exception)
            {
                Console.WriteLine("Invalid Input!!!");
                return "NA";
            }
        }

        public static string takeList() 
        {
            string s = "";
            while(s == "")
            {
                Console.Write("Enter the series of numbers separated by comma: ");
                s = Console.ReadLine();
                s = s.Replace(" ", "");
                if(s == "")
                {
                    Console.WriteLine("Invalid List");
                }
            }
            string[] arr = s.Split(",");
            string threeItems = getSmallestthreeItem(arr);
            if(threeItems == "InvalidListException")
            {
                    threeItems = takeList();
            }
                return threeItems;
        }

    }

    public class Problem4 
    {

        private static string sort(string[] arr) 
        {
            try
            {
                for(int i = 0; i < arr.Length - 1; i++) 
                {
                    for(int j = 0; j < arr.Length - i - 1; j++)
                    {
                        checked
                        {
                            if(Convert.ToDouble(arr[j]) < Convert.ToDouble(arr[j + 1]))
                            {
                                string temp = arr[j];
                                arr[j] = arr[j + 1];
                                arr[j + 1] = temp;
                            }
                        }
                    }
                }
                return string.Join(", ", arr);
            }
            catch(System.Exception)
            {
                Console.WriteLine("Invalid Input!");
                return "NA";
            }
        }

        public static string takeList() 
        {
            try 
            {
                string s;
                Console.Write("Enter the series of numbers separated by comma: ");
                s = Console.ReadLine();
                s = s.Replace(" ", "");
                if(s == "")
                    throw new Exception();
                string[] arr = s.Split(",");
                string sortedList = sort(arr);
                return sortedList;
            }
            catch(System.Exception) {
                Console.WriteLine("Invalid Input!");
                return "NA";
            }
        }       

    }

}