namespace MyApp2
{
    public class AdvancedCalculator : Calculator
    {

        public override string GetResult()
        {
            string temp = Result;
            Result = "NaN";
            return temp;
        }

        public void Power(string base_val, string expo_val)
        {
            try
            {
                checked
                {
                    double s = 1;
                    int b = Convert.ToInt32(base_val); 
                    int limit = Convert.ToInt32(expo_val);
                    if(limit == 0)
                    {
                        SetResult("1");
                        return;
                    }
                    else if(limit < 0) {
                        for(int i = -1; i >= limit; i--)
                        {
                            s /= b;
                        }
                    }
                    else 
                    {
                        for (int i = 1; i <= limit; i++)
                        {
                            s *= b;
                        }
                    }
                    if(limit < 0)
                    {
                        s *= 1000000;
                        SetResult(Convert.ToString(s) + " micros");
                        return;
                    }
                    string temp = Convert.ToString(Convert.ToInt32(s));
                    SetResult(temp.PadRight(temp.Length + 6, '0') + " micros");
                    return;
                }
            }
            catch (System.Exception)
            {
                System.Console.WriteLine("Invalid argument");
                return;
            }
        }
    }
}