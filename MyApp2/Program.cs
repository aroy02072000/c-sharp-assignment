﻿using System;

namespace MyApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            char Choice;
            while(true)
            {
                System.Console.Write("Enter the value of a: ");
                string a = Console.ReadLine();
                System.Console.Write("Enter the value of b: ");
                string b = Console.ReadLine();
                System.Console.Write("Enter the value of c: ");
                string c = Console.ReadLine();
                Calculator calculator = new Calculator();
                calculator.Add(a, b);
                System.Console.WriteLine($"Sum is {calculator.GetResult()}");
                calculator.Add(a, b, c);
                System.Console.WriteLine($"Sum is {calculator.GetResult()}");       
                AdvancedCalculator advancedCalculator = new AdvancedCalculator();
                advancedCalculator.Power(a, b);
                System.Console.WriteLine($"{a}^{b} = {advancedCalculator.GetResult()}");
                System.Console.Write("Do you want to exit(Y / Press any key): ");
                Choice = System.Console.ReadKey().KeyChar;
                System.Console.WriteLine();
                if(Choice == 'Y' || Choice == 'y')
                    break;
            }
        } 
    }
}