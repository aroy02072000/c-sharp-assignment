namespace MyApp2
{
    public class Calculator : IOperations
    {
        // stores the sum in Result variable
        protected string Result = "NaN";
        
        // ExceptionOccured variable keeps track whether any exception has occured or not
        private bool ExceptionOccured = false;

        // setter method of Result
        protected void SetResult(string val)
        {
            Result = val;
        }

        // getter method of Result
        public virtual string GetResult()
        {
            string temp = Result;

            // resetting the values of the variable
            ExceptionOccured = false;
            
            Result = "NaN";
            return temp;
        }

        // void Add(string, string) can add two integer or float values 
        public void Add(string a, string b)
        {
            try
            {
                checked
                {
                    // if both the variables are float then proceed
                    if(a.Contains(".") && b.Contains("."))
                    {
                        Result = Convert.ToString(float.Parse(a) + float.Parse(b));
                    }
                    
                    // otherwise both the variables are int
                    else 
                    {
                        Result = Convert.ToString(int.Parse(a) + int.Parse(b));
                    }
                }
            }
            catch(Exception)
            {
                ExceptionOccured = true;
                Result = "NaN";
                Console.WriteLine("Invalid Argument!!!");
                return;
            }
        }

        public void Add(string a, string b, string c)
        {
            // if three variables are float then return
            if(a.Contains(".") && b.Contains(".") && c.Contains(".")) { return; }
            
            Add(a, b);
            
            // if any kind of exception occurs in previous callback at line 50 then don't proceed
            if(ExceptionOccured) { return; }
            
            Add(Result, c);
        }
    }
}